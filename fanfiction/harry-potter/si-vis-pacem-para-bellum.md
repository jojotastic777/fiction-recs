# [Fanfiction](../index.md) / [Harry Potter](./index.md) / [Si Vis Pacem, Para Bellum, by Mister Cynical](#)

## Link
https://www.fanfiction.net/s/12302907/1/Si-Vis-Pacem-Para-Bellum

## Author Summary
All they wanted was a normal school year and now they're willing to fight for it. An alternative take on the Trio's fourth year.

## Reader Summary
In which Harry Potter, Hermione Granger, and Ronald Weasley would *really like* just one, normal, *non-life-threatening* year at Hogwarts, and take matters into their own hands to try and get it. Unfortunately for them, the year is 1994, the Triwizard Tournament has come to Hogwarts, and the Dark Lord has *plans* for Harry which mean he's going to have to participate whether he likes it or not.

As for *how* the three indend to ensure a normal year?

Gratuitous amounts of violence, an a "healthy" dose of paranoia, of course!

## Contains:
- Badass!Trio (& Co.)
- "Hermione is scaaaary because she reads."
- An extremely tight friendship between Harry, Ron, and Hermione.
- Luna is a major character.
- Cooperation between Triwizard competetors.